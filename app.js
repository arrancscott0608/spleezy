const Nightmare = require('nightmare');
let _ = require('lodash');
// var config = require('./config');
let config = require('./config-test-splash');
let util = require('util');
let fs = require('fs');

// Read in proxies from test file
let data = fs.readFileSync('./proxies.txt', 'utf8');
let proxies = data.split('\n');
let userAgent = config.userAgent;
let url = config.url;
let nightmareArr = [];
let count = 0;

// Add custom action to show electron window
Nightmare.action('show',
    function (name, options, parent, win, renderer, done) {
        parent.respondTo('show', function (done) {
            win.maximize();
            win.show();
            done();
        });
        done();
    },
    function (done) {
        this.child.call('show', done);
    });

_.each(proxies, function (proxy, index) {
    nightmareArr.push(
        Nightmare({
            switches: {
                'proxy-server': 'socks5://' + proxy
            },
            show: false
        })
    );

    spleezy(nightmareArr[index], proxy)
});

function spleezyInterval() {
    // Check existing sessions. If passed splash, open browser
    setInterval(function () {
        console.log('nightmareArr length: ' + nightmareArr.length);
        _.each(nightmareArr, function (nightmare) {
            nightmare
                .exists(config.uniqueSelector)
                .then(function (elementExists) {
                    if (elementExists) {
                        console.log('No luck, trying again in 10 seconds');
                        nightmare
                            .cookies.clearAll()
                            .refresh();
                    } else {
                        // Through splash, yay!
                        console.log('Through splash!');
                        removeFromNightmareArr(nightmare);
                        return nightmare.show();
                    }
                })
        });
    }, parseInt(config.intervalSeconds) * 1000);
}

function spleezy(nightmare, proxy) {
    nightmare
        .useragent(userAgent)
        .goto(url)
        .wait('.gallery-cart-wrapper')
        .exists(config.uniqueSelector)
        .then(function (elementExists) {
            count += 1;
            if (count === proxies.length) spleezyInterval();
            if (elementExists) {
                console.log('No luck on proxy: ' + proxy);
                nightmare
                    .cookies.clearAll()
                    .refresh();
            } else {
                // Through splash, yay!
                console.log('Through splash on first pass, woo!');
                return nightmare.show();
            }
        })
        .catch(function (error) {
            count += 1;
            console.error('an error has occurred: ' + error);
            console.error(util.inspect(error));
            removeFromNightmareArr(nightmare);
            return nightmare.end();
        });
}

function removeFromNightmareArr(nightmare) {
    delete nightmareArr[nightmareArr.indexOf(nightmare)];
    nightmareArr = _.without(nightmareArr, undefined);
}
