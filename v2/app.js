// class Spleezy {
//     constructor(userAgent, url, selector, interval) {
//         // Read in proxies from test file
//         let data = fs.readFileSync('./proxies.txt', 'utf8');
//         this.proxies = data.split('\n');
//         this.userAgent = userAgent;
//         this.url = url;
//         this.selector = selector;
//         this.interval = parseInt(interval) * 1000;
//         this.nightmareArr = [];
//         this.count = 0;
//     }
//     run() {
//         _.each(this.proxies, function (proxy, index) {
//             this.nightmareArr.push(
//                 Nightmare({
//                     switches: {
//                         'proxy-server': 'socks5://' + proxy
//                     },
//                     show: false
//                 })
//             );
//
//             this.spleezy(nightmareArr[index], proxy)
//         });
//     }
//     spleezy(nightmare, proxy) {
//         nightmare
//             .useragent(this.userAgent)
//             .goto(this.url)
//             .wait('.gallery-cart-wrapper')
//             .exists(this.selector)
//             .then(function (elementExists) {
//                 this.count += 1;
//                 if (this.count === this.proxies.length) this.spleezyInterval();
//                 if (elementExists) {
//                     console.log('No luck on proxy: ' + proxy);
//                     nightmare
//                         .cookies.clearAll()
//                         .refresh();
//                 } else {
//                     // Through splash, yay!
//                     console.log('Through splash on first pass, woo!');
//                     return nightmare.show();
//                 }
//             })
//             .catch(function (error) {
//                 this.count += 1;
//                 console.error('an error has occurred: ' + error);
//                 console.error(util.inspect(error));
//                 this.removeFromNightmareArr(nightmare);
//                 return nightmare.end();
//             });
//     }
//     spleezyInterval() {
//         // Check existing sessions. If passed splash, open browser
//         setInterval(function () {
//             console.log('nightmareArr length: ' + this.nightmareArr.length);
//             _.each(this.nightmareArr, function (nightmare) {
//                 nightmare
//                     .exists(this.selector)
//                     .then(function (elementExists) {
//                         if (elementExists) {
//                             console.log('No luck, trying again in 10 seconds');
//                             nightmare
//                                 .cookies.clearAll()
//                                 .refresh();
//                         } else {
//                             // Through splash, yay!
//                             console.log('Through splash!');
//                             this.removeFromNightmareArr(nightmare);
//                             return nightmare.show();
//                         }
//                     })
//             });
//         }, this.interval);
//     }
//     removeFromNightmareArr(nightmare) {
//         delete this.nightmareArr[this.nightmareArr.indexOf(nightmare)];
//         this.nightmareArr = _.without(this.nightmareArr, undefined);
//     }
// }
//
// let spleezy = new Spleezy(
//     config.userAgent,
//     config.url,
//     config.uniqueSelector,
//     config.intervalSeconds
// );
//
// spleezy.run();
